{ ... }:
{
  imports = [ ./default.nix ];
  services.xserver.xkb.variant = "workman";
}
