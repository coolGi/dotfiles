{ ... }:
{
  i18n.supportedLocales = [
    "en_AU.UTF-8/UTF-8"
    "en_US.UTF-8/UTF-8"
    "ja_JP.UTF-8/UTF-8"
    "tok/UTF-8"
  ];
}
