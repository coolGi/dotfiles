{ ... }:
{
  imports = [
    ./../../home-manager/home.nix

    ./../../home-manager/flakes/stylix
    ./../../home-manager/flakes/nixvim
    #./../../home-manager/flakes/nvf
    #./../../home-manager/flakes/ags
  ];
}
